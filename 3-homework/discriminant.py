# введите коэффиценты  уравнения

cf = []
for i in ['a', 'b', 'c']:
    cf.append(input(f"Введите коэффицент({i}): "))

# Преобразования комплексных чисел

for i in range(0, 3):
    cf[i] = complex(''.join(cf[i].split()))
    if cf[i].imag == 0:
        cf[i] = cf[i].real

a = cf[0]
b = cf[1]
c = cf[2]

# Корни
x1 = 1 / (2 * a) * (-b + (b**2 - 4 * a * c)**(1 / 2))
x2 = 1 / (2 * a) * (-b - (b**2 - 4 * a * c)**(1 / 2))

if x1 == x2:
    print(f"Корень уравнения будет: {x1}")
else:
    print(f"Корни уравнения: {x1,x2}")
