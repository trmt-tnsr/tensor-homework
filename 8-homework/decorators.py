from time import perf_counter,sleep
from random import randint

def log_decorator(func):
    """
    Декоратор логов
    """
    def wrapper(n):
        func(n)
        return func.__name__, n
    return wrapper

def time_decorator(func):
    """
    Функция которая возвращает время выполнение функции
    """
    def wrapper(n) -> float:
        launch_time = perf_counter()
        func(n)
        return perf_counter() - launch_time
    return wrapper


def slow_decorator(func):
    """
    Функция которая замедляет работу функции
    """
    def wrapper(n):
        func(n)
        sleep(1)
    return wrapper

def frequency(func):
    """
    Функция которая ограничивает частоту вызова функции
    """
    last = [perf_counter()]
    def decorator(n):
        if perf_counter() - last[0] < 10:
            raise RuntimeError("Пожалуйста подождите...")
        last[0] = perf_counter()
        return func(n)
    return decorator

def wallet_magic(n: int)->int:
    """
    Функция которое увеличивает число в пять раз если оно единица
    """
    i = 0
    x = 1
    while i < n:
        x = 4*i+5
        i += 1
    return x

tdr = time_decorator(wallet_magic)
ldr = log_decorator(wallet_magic)
sdr = time_decorator(slow_decorator(wallet_magic))

print(f"Время выполнение функции {tdr(10)} с")
print(f"Функция - {ldr(randint(1,100))}")
print(f"Время выполнение после замедления {sdr(10)} с")

fdr = frequency(wallet_magic)
sleep(11)
k = randint(1,100)
print(f"Частота вызова функции - {fdr(k)}, для значения {k}")
