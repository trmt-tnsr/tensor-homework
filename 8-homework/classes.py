class Animals:
    def __init__(self, nutrition, vertebrae, age):
        self.nutrition = nutrition
        self.vertebrae = vertebrae
        self.age = age
    def how(self):
        print(f"Это животное живущее {self.age} лет и являющаяся {self.vertebrae}, питается {self.nutrition} ")

class Mammals(Animals):
    def __init__(self, nutrition, vertebrae, age,weight,height,gender):
        super().__init__(nutrition,vertebrae,age)
        self.weight = weight
        self.height = height
        self.gender = gender
    def basic_parameters(self):
        print(f"Млекопитающее, которое имеет рост {self.height}, вес {self.weight} и являются {self.nutrition}.")
    def gender_view(self):
        print(f"Пол: {self.gender}")

class Cat(Mammals):
    def __init__(self, nutrition, vertebrae, age,weight,height,gender,jump_height,claw_length,nick):
        super().__init__(nutrition, vertebrae, age,weight,height,gender)
        self.claw_length = claw_length
        self.nick = nick
        self.jump_height = jump_height
    def info(self):
       print(f"Кот по кличке {self.nick}, прыгает на расстояние {self.jump_height}")
    def claw(self):
        print(f"Длина когтей {self.claw_length}")



class Dog(Mammals):
    def __init__(self, nutrition, vertebrae, age,weight,height,gender,family,nick):
        super().__init__(nutrition, vertebrae, age,weight,height,gender)
        self.family = family
        self.nick = nick
    def type_of(self):
        print(f"Собака принадлежит {self.family}")
    def who(self):
        print(f"Пес по кличке {self.nick}")

class Human(Mammals):
    def __init__(self, nutrition, vertebrae, age,weight,height,gender,iq,eye_color,name):
        super().__init__(nutrition, vertebrae, age,weight,height,gender)
        self.iq = iq
        self.eye_color = eye_color
        self.name = name
    def get_number_of_iq(self):
        print(f"Количество единиц iq: {self.iq}")

class Student(Human):
    def __init__(self, nutrition, vertebrae, age,weight,height,gender,iq,eye_color,name,number_of_delivered_homeworks,course):
        super().__init__(nutrition, vertebrae, age,weight,height,gender,iq,eye_color,name)
        self.number_of_delivered_homeworks  = number_of_delivered_homeworks
        self.course = course

    def  get_course(self):
        print(f"Студент {self.name} на {self.course} курсе")

    def __eq__(self,student):
        return self.number_of_delivered_homeworks == student.number_of_delivered_homeworks


# Получить информацию о коте

Cat("carnivorous","Mammals",10,10,"female",100,"10 cm",100,"rick").info()

# Сравнение
st1 = Student("carnivorous","mammal",20,90,180,"male",100,"black","blair",10,1)
st2 = Student("carnivorous","mammal",21,60,175,"female",150,"black","elfriede",5,2)

print(st1 == st2)

# получить курс для первого студента
st1.get_course()

