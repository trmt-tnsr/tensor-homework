def add(a: int, b: int) -> int:
    """
    Функция принимает два целых числа и возвращает их сумму
    """
    return a + b


def prod(*a: float) -> (float | ValueError):
    """
    Функция принимает n-чисел и возвращает их произведение
    """
    try:
        s = 1
        for sn in a:
            s = s * sn
        return s
    except ValueError or TypeError as e:
        return e


def awesome() -> str:
    """
    Функция которая возвращает фразу
    """
    return "прекрасно"
