from os import uname, getlogin, listdir

print(f"Имя операционной системы {uname().sysname}")
print(f"Имя пользователя {getlogin()}")
print(f"Список текущих файлов и директорий {listdir('.')}")
