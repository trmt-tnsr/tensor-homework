К последнему заданию

1. Посмотрели пакеты установленные в системе и сделали виртуальное окружение

![pip instal and activate venv](./images/1-check-pip-install.jpg)

2. Установили пакет `fastapi` и сопутствующие, вышли из виртуального окружения и
   убедились в том, что пакет установлен только в нем

![deactivate venv after install fastapi and check deps](./images/2-check-after-deactivate-env.jpg)
