from numpy import ndarray
from numpy.random import default_rng

a: ndarray = default_rng(42).random((3, 3))

print(f"Массив со случайными значениями arr = {a}")
