from random import randrange

print("Угадай число")

secret = randrange(1, 100, 1)

while True:
    number = int(
        input("Напиши свое любимое число сюда, или введите 0 для остановки:"))
    if number == 0:
        break

    print("Итак ваше число", number)

    if secret < number:
        print("Введеное число больше загаданного")
        continue
    elif secret == number:
        print("Вы угадали")
        break
    elif secret > number:
        print("Введенное число меньше загаданного")
        continue
    else:
        print("Что то пошло не так, вот только что...")
        break
