try:
    a = float(input("Введите первое число: "))
    b = float(input("Введите второе число: "))

    print(f"Сложение: a+b = {a+b}")
    print(f"Вычитание: a-b = {a-b}")
    print(f"Умножение: a*b = {a*b}")
    print(f"Деление: a/b = {a/b}")
    print(f"Возведение в степень: a^b = {a**b} или b^a = {b**a}")
    print(f"Деление по модулю: a % b = {a%b}")
    print(f"Целочисленное деление: a//b={int(a//b)}")
except ValueError or TypeError:
    print("Ошибка, введено не число")
