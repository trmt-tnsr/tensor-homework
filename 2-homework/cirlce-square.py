from math import pi

r = input("Введите радиус круга(через пробел можно указать единицы измерения): ")

if ' ' in r:
    r = r.split(" ")
    print(f"Площадь круга(в {str(r[1])}): {pi*float(r[0])**2}")
else:
    print(f"Площадь круга: {pi*float(r)**2}")
