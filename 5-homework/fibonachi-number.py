def fibo(num: int):
    """
    функция которое принимает число и возвращает число фибоначи соответствующее ему
    """
    if num == 0:
        return 0
    elif num == 1 or num == 2:
        return 1
    elif num < 0:
        print("Неправильный ввод")
    else:
        return fibo(num - 1) + fibo(num - 2)


try:
    a: int = int(input("введите любое число: "))
    print(f"число фибоначи тогда: {fibo(a)}")
except ValueError or TypeError:
    print("неверное число")
