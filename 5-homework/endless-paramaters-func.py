
def func(*a: float) -> (float | ValueError):
    """
    Функция принимает на вход сколько угодно аргументов и возвращает их сумму
    """
    try:
        s = 0
        for sn in a:
            s = s + sn
        return s
    except ValueError or TypeError as e:
        return e


assert func(2, 3, 4, 5, 6, 7) == 27, "что то пошло не так"
assert func(1, 2, 3) == 6, "что то пошло не так"

print(
    f"сумма чисел, например 19875+1891+1819+1578 = {func(19875,1891,1819,1578)}")
