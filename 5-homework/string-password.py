def string_pass(string: str) -> bool:
    """
    функция принимает на вход строку и проверяет пароль, возвращая булево значение
    """
    try:
        if len(string) < 6:
            raise Exception("пароль должен быть не менее 6 символов")
        elif string.isdigit():
            raise Exception("пароль не должен состоять из цифр")
        elif "password" in string.lower():
            raise Exception(
                "пароль не должен содержать слово password в любом регистре")
        elif not any(chr.isdigit() for chr in string):
            raise Exception("пароль должен содержать хотя бы одну цифру")
        return True
    except Exception as e:
        print(e)
        return False


print("Тесты...")
assert string_pass(
    "gfs") == False, "пароль должен быть не менее 6 символов, меньше и ничего"
assert string_pass(
    "2347981") == False, "пароль не должен состоять из цифр, но ведь работает"
assert string_pass(
    "hgfjkfhhg") == False, "пароль должен содержать хотя бы одну цифру, но ведь работает"
assert string_pass(
    "password484") == False, "пароль не должен содержать слово password, а у функции все хорошо"
assert string_pass(
    "PASSWORD35") == False, "пароль не должен содержать слово password в любом регистре, а функция проходит"
assert string_pass(
    "pASswORd1987") == False, "пароль не должен содержать слово password в любом регистре, а функция проходит"
assert string_pass(
    "jfhHJhJhHkl3ji") == True, "пароль верный, а функция выдает что не верный"
assert string_pass(
    "jfhfhJhhklji316") == True, "пароль верный, а функция выдает неверный"

print("Не обращайте внимания, это тесты, у вас все возможно хорошо")

print(string_pass(input("Введите ваш пароль: ")))
