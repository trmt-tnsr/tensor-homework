
# Преобразования комплексных чисел
def solve_quadric(cf: list):
    for i in range(0, 3):
        cf[i] = complex(''.join(cf[i].split()))
        if cf[i].imag == 0:
            cf[i] = cf[i].real

    a = cf[0]
    b = cf[1]
    c = cf[2]

    if a == 0 and b ==0 and c !=0:
        return "Уравнение не имеет решений"
    elif a == 0 and c == 0:
        return 0.0
    elif a == 0:
        return -c/b

    # Корни
    x1 = 1 / (2 * a) * (-b + (b**2 - 4 * a * c)**(1 / 2))
    x2 = 1 / (2 * a) * (-b - (b**2 - 4 * a * c)**(1 / 2))

    if x1 == x2:
        return x1
    else:
        return x1,x2
