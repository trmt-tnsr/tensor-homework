from discriminant import solve_quadric

def test_0():
    assert solve_quadric(['0','0','0']) == 0, "Должен быть ноль"
    assert solve_quadric(['0','1','2']) == -2.0, "значение -2.0, что то пошло не так"
    assert solve_quadric(['0','0','1']) == "Уравнение не имеет решений", "уравнение не дожно иметь решений"
def test_1():
    assert solve_quadric(['0','1','0']) == 0, "тоже 0, а у вас другое число"
    assert solve_quadric(['1','2','3']) == ((-0.9999999999999999+1.4142135623730951j), (-1-1.4142135623730951j)),"должны быть значения ((-0.9999999999999999+1.4142135623730951j), (-1-1.4142135623730951j))"
    assert solve_quadric(['2','1','0']) == (0.0, -0.5),"у вас тоже другие числа"
    assert solve_quadric(['2','1','0']) == "no roots"

test_0()
test_1()
