from sys import argv

key: list = list(input('введите ключ: '))

with open('phrase.txt', 'r') as input_file:
    text: list = list(input_file.read())
    for i in range(len(text)):
        text[i] = ord(text[i]) ^ ord(key[i % len(key)])

encoded_text: str = ''

if argv[-1] != '-d' or argv[-1] != '--decode':
    for t in text:
        encoded_text += chr(t)
    with open('encode-phrase.txt', 'w') as output_file:
        output_file.write(encoded_text)

else:
    for i in range(len(text)):
        text[i] = chr(text[i] ^ ord(key[i % len(key)]))

    with open('decoded-phrase.txt', 'w') as output_file:
        output_file.write(''.join(text))
