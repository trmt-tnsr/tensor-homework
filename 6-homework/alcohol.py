with open("moleculas.txt", "r") as inpt:
    try:
        c, h, o = inpt.read().split()
        c = int(c)
        h = int(h)
        o = int(o)
        if c < 2 or h < 6 or o < 1:
            max = 0
        else:
            max = min(c // 2, h // 6, o // 1)
    except ValueError or TypeError:
        print("Количество молекул должно быть целым числом")

with open("connection.txt", "w") as f:
    f.write("Количество молекул спирта - " + str(max))
