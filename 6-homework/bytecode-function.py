def list_encode(list: list) -> list:
    """
    Декодирование всех строк, на вход подается список и возвращается список
    """
    for i in range(len(list)):
        list[i] = list[i].encode()
    return list


def list_decode(list: list) -> list:
    """
    Раскодирование всех строк, на вход подается список и возвращается список
    """
    for i in range(len(list)):
        list[i] = list[i].decode()
    return list


list = ['python', 'typescript', 'lua', 'shell']

print(f"Оригинальный список: {list}")
print(f"Список закодированных строк: {list_encode(list)}")
print(f"Список раскодированных строк: {list_decode(list)}")
