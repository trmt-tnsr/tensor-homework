inventory: dict = {
    "usdc": 60,
    "gazp": 164,
    "fil": 300
}

sum_inventory: int = 2000

while True:
    backpack: str = input(
        "Добавить элемент или удалить, посмотреть портфель и выйти из программы(a/d/p/e): ")
    if backpack == 'a':
        k: str = input("Введите название валюты: ")
        v: int = int(input("Введите текущую цену: "))
        if k in inventory:
            inventory[k] = inventory[k] + v
        else:
            inventory[k] = v
        c: int = sum(inventory.values())
        if c > sum_inventory:
            print(
                "Приносим свои извенения, но портфель переполнен, поэтому мы вынуждены обнулить валюту")
            if k in inventory:
                inventory[k] = inventory[k] - v
            else:
                inventory[k] = 0
            continue
        continue
    elif backpack == 'd':
        k: str = input("Введите название предмета: ")
        del inventory[k]
        continue
    elif backpack == 'p':
        for k, v in inventory.items():
            print(f"{v} монет {k} валюты")
        continue
    elif backpack == 'e':
        print("Заканчиваем работу")
        break
