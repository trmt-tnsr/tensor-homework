from random import randrange

a: set = set()
b: set = set()

for i in range(10**2):
    a.add(randrange(10, 10**3))
    b.add(randrange(10, 10**3))
    i *= 5

print(f"наши наборы:\n первый: {a}\n второй: {b}")

k: dict = dict({
    "оба списка": [],
    "в первое": [],
    "во второе": [],
    "неодновременно": [],
})

for k1 in a:
    for k2 in b:
        if k1 == k2:
            k["оба списка"].append(k1)

k["в первое"] = a.difference(b)
k["во второе"] = b.difference(a)
k["неодновременно"] = a.symmetric_difference(b)


print(f"элементы { k['оба списка'] } входят в оба кортежа")
print(f"элементы { k['в первое'] }\n входят в первый кортеж")
print(f"элементы { k['во второе'] }\n входят во второй кортеж")
print(
    f"элементы { k['неодновременно'] }\n входят и в первый и во второй кортеж неодновременно")
