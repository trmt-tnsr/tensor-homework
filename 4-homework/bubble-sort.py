def bubble_sort(arr: list) -> list:
    """
    алгоритм пузырьковый сортировки принимает список и возвращает его
    """
    n = len(arr)

    for i in range(n):
        for j in range(0, n - i - 1):
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]

    return arr


arr: list = [[5, 1, 4, 2, 8], [4, -7788, 0, -8484, -1]]
sort_arr: list = [[1, 2, 4, 5, 8], [-8484, -7788, -1, 0, 4]]


# протестируем результат
assert bubble_sort(
    arr[0]) == sort_arr[0], f"массив отсортован неправильно, нужно: {sort_arr[0]}"
assert bubble_sort(
    arr[1]) == sort_arr[1], f"массив отсортован неправильно, нужно: {sort_arr[1]}"

# если все хорошо то выводим

sort_arr: list = bubble_sort(arr[0])

print(f"отcортированный массив: { sort_arr }")
