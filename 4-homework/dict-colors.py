colors: dict = {
    "red": (255, 0, 0),
    "orangered": (255, 69, 0),
    "aqua": (0, 255, 255),
    "blue": (0, 0, 255),
    "skyblue": (135, 206, 235)
}
